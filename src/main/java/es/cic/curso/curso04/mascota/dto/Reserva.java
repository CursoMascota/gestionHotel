package es.cic.curso.curso04.mascota.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import es.cic.curso.curso04.mascota.repository.Identificable;

@Entity
public class Reserva implements Identificable<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "tipo_pension")
	private String tipoPension;

	@JoinColumn(name = "idHabitacion")
	@ManyToOne(fetch = FetchType.LAZY)
	private Habitacion habitacion;

	@JoinColumn(name = "idCliente")
	@ManyToOne(fetch = FetchType.LAZY)
	private Cliente cliente;

	private String checkIn;
	private String checkOut;
	private String extras;
	private String peticiones;

	@Column(name = "precio_total")
	private double precioTotal;

	public Reserva() {
		super();
	}

	public Reserva(String tipoPension, Habitacion habitacion, Cliente cliente, String checkIn, String checkOut,
			String extras, String peticiones, double precioTotal) {
		super();
		this.tipoPension = tipoPension;
		this.habitacion = habitacion;
		this.cliente = cliente;
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.extras = extras;
		this.peticiones = peticiones;
		this.precioTotal = precioTotal;
	}

	public Reserva(Long id, String tipoPension, Habitacion habitacion, Cliente cliente, String checkIn, String checkOut,
			String extras, String peticiones, double precioTotal) {
		super();
		this.id = id;
		this.tipoPension = tipoPension;
		this.habitacion = habitacion;
		this.cliente = cliente;
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.extras = extras;
		this.peticiones = peticiones;
		this.precioTotal = precioTotal;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoPension() {
		return tipoPension;
	}

	public void setTipoPension(String tipoPension) {
		this.tipoPension = tipoPension;
	}

	public Habitacion getHabitacion() {
		return habitacion;
	}

	public void setHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public String getExtras() {
		return extras;
	}

	public void setExtras(String extras) {
		this.extras = extras;
	}

	public String getPeticiones() {
		return peticiones;
	}

	public void setPeticiones(String peticiones) {
		this.peticiones = peticiones;
	}

	public double getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(double precioTotal) {
		this.precioTotal = precioTotal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reserva other = (Reserva) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Reserva [id=" + id + ", tipoPension=" + tipoPension + ", habitacion=" + habitacion + ", cliente="
				+ cliente + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", extras=" + extras + ", peticiones="
				+ peticiones + ", precioTotal=" + precioTotal + "]";
	}

}
