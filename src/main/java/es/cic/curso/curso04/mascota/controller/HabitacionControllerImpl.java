package es.cic.curso.curso04.mascota.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso04.mascota.dto.Habitacion;
import es.cic.curso.curso04.mascota.repository.HabitacionRepository;
import es.cic.curso.curso04.mascota.service.HabitacionService;

@Controller
@Transactional
public class HabitacionControllerImpl implements HabitacionController {

	@Autowired
	HabitacionRepository habitacionRepository;

	@Autowired
	HabitacionService habitacionService;

	@Override
	public List<Habitacion> listar() {
		List<Habitacion> listaHabitaciones = new ArrayList<>();
		listaHabitaciones = habitacionService.listar();
		return listaHabitaciones;
	}

	@Override
	public Habitacion insertarHabitacion(int numHabitacion, String tipoHabitacion, int numPersonas, boolean isOcupada) {
		Habitacion nuevaHabitacion = habitacionService.insertarHabitacion(numHabitacion, tipoHabitacion, numPersonas,
				isOcupada);

		return nuevaHabitacion;
	}

}
