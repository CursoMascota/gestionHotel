package es.cic.curso.curso04.mascota.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso04.mascota.dto.Cliente;
import es.cic.curso.curso04.mascota.repository.ClienteRepository;

@Service
@Transactional
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public Cliente altaCliente(String nombre, String apellidos, String NIF, int telefono) {
		Cliente clienteNuevo = new Cliente(nombre, apellidos, NIF, telefono);
		return clienteRepository.add(clienteNuevo);

	}

	@Override
	public void borrarCliente(Long id) {
		Cliente clienteABorrar = clienteRepository.read(id);
		clienteRepository.delete(clienteABorrar);

	}
}
