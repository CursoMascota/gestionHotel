package es.cic.curso.curso04.mascota.repository;

import es.cic.curso.curso04.mascota.dto.Reserva;

public interface ReservaRepository extends IRepository<Long, Reserva> {

}
