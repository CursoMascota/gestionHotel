package es.cic.curso.curso04.mascota.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso04.mascota.dto.Reserva;

@Repository
@Transactional
public class ReservaRepositoryImpl extends AbstractRepositoryImpl<Long, Reserva> implements ReservaRepository {

	@Override
	public Class<Reserva> getClassDeT() {
		return Reserva.class;
	}

	@Override
	public String getNombreTabla() {
		return "reserva";
	}

}
