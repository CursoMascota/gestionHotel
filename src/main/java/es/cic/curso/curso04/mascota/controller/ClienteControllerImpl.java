package es.cic.curso.curso04.mascota.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso04.mascota.dto.Cliente;
import es.cic.curso.curso04.mascota.service.ClienteService;

@Controller
@Transactional
public class ClienteControllerImpl implements ClienteController {

	@Autowired
	private ClienteService clienteService;

	@Override
	public Cliente altaCliente(String nombre, String apellidos, String NIF, int telefono) {
		Cliente cliente = clienteService.altaCliente(nombre, apellidos, NIF, telefono);
		return cliente;
	}

	@Override
	public void borrarCliente(Long id) {
		clienteService.borrarCliente(id);

	}
}
