package es.cic.curso.curso04.mascota.service;

import java.util.List;

import es.cic.curso.curso04.mascota.dto.Habitacion;

public interface HabitacionService {

	List<Habitacion> listar();

	Habitacion insertarHabitacion(int numHabitacion, String tipoHabitacion, int numPersonas, boolean isOcupada);

}