package es.cic.curso.curso04.mascota.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso04.mascota.dto.Habitacion;

@Repository
@Transactional
public class HabitacionRepositoryImpl extends AbstractRepositoryImpl<Long, Habitacion> implements HabitacionRepository {

	@Override
	public Class<Habitacion> getClassDeT() {
		return Habitacion.class;
	}

	@Override
	public String getNombreTabla() {
		return "habitacion";
	}
}
