package es.cic.curso.curso04.mascota.service;

import es.cic.curso.curso04.mascota.dto.Cliente;

public interface ClienteService {

	void borrarCliente(Long id);

	Cliente altaCliente(String nombre, String apellidos, String NIF, int telefono);

}
