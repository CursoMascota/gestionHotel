package es.cic.curso.curso04.mascota.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso04.mascota.dto.Cliente;
import es.cic.curso.curso04.mascota.dto.Habitacion;
import es.cic.curso.curso04.mascota.dto.Reserva;
import es.cic.curso.curso04.mascota.service.ReservaService;

@Controller
@Transactional
public class ReservaControllerImpl implements ReservaController {

	@Autowired
	private ReservaService reservaService;

	@Override
	public Reserva reservar(String tipoPension, Habitacion habitacion, Cliente cliente, String checkIn, String checkOut,
			String extras, String peticiones) {
		Reserva reservaNueva = reservaService.reservar(habitacion.getTipoHabitacion(), habitacion, cliente, checkIn,
				checkOut, extras, peticiones);
		return reservaNueva;
	}

	@Override
	public Habitacion ObtenerHabitacion(String tipoHabitacion, List<Habitacion> listaHabitaciones) {
		Habitacion habitacionAReservar = reservaService.ObtenerHabitacion(tipoHabitacion, listaHabitaciones);
		return habitacionAReservar;
	}

	@Override
	public boolean comprobarOcupada(Habitacion habitacion) {
		boolean ocupada = reservaService.comprobarOcupada(habitacion);

		return ocupada;
	}

	@Override
	public double calcularPrecio(double precio, int numNoches) {
		double precioTotal = reservaService.calcularPrecio(precio, numNoches);
		return precioTotal;
	}

	@Override
	public void borrarReserva(Long id) {
		reservaService.borrarReserva(id);

	}

}
