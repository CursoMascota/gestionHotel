package es.cic.curso.curso04.mascota.repository;

import es.cic.curso.curso04.mascota.dto.Habitacion;

public interface HabitacionRepository extends IRepository<Long, Habitacion> {

}
