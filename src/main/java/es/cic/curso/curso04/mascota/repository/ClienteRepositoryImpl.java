package es.cic.curso.curso04.mascota.repository;

import org.springframework.stereotype.Repository;

import es.cic.curso.curso04.mascota.dto.Cliente;

@Repository
public class ClienteRepositoryImpl extends AbstractRepositoryImpl<Long, Cliente> implements ClienteRepository {

	@Override
	public Class<Cliente> getClassDeT() {
		return Cliente.class;
	}

	@Override
	public String getNombreTabla() {
		return "cliente";
	}

}
