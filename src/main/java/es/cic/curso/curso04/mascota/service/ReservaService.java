package es.cic.curso.curso04.mascota.service;

import java.util.List;

import es.cic.curso.curso04.mascota.dto.Cliente;
import es.cic.curso.curso04.mascota.dto.Habitacion;
import es.cic.curso.curso04.mascota.dto.Reserva;

public interface ReservaService {

	double calcularPrecio(double precio, int numNoches);

	void borrarReserva(Long id);

	boolean comprobarOcupada(Habitacion habitacion);

	Reserva reservar(String tipoHabitacion, Habitacion habitacion, Cliente cliente, String checkIn, String checkOut,
			String extras, String peticiones);

	Habitacion ObtenerHabitacion(String tipoHabitacion, List<Habitacion> listaHabitaciones);

}
