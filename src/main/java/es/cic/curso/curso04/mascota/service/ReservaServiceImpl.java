package es.cic.curso.curso04.mascota.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso04.mascota.dto.Cliente;
import es.cic.curso.curso04.mascota.dto.Habitacion;
import es.cic.curso.curso04.mascota.dto.Reserva;
import es.cic.curso.curso04.mascota.repository.HabitacionRepository;
import es.cic.curso.curso04.mascota.repository.ReservaRepository;

@Service
@Transactional
public class ReservaServiceImpl implements ReservaService {

	private static final double PRECIO_NOCHE = 50.0;

	@Autowired
	private ReservaRepository reservaRepository;

	@Autowired
	private HabitacionRepository habitacionRepository;

	@Override
	public Reserva reservar(String tipoPension, Habitacion habitacion, Cliente cliente, String checkIn, String checkOut,
			String extras, String peticiones) {
		String tipoHabitacion = habitacion.getTipoHabitacion();
		Reserva reservaNueva = new Reserva();
		short numNoches = 0;
		boolean ocupada;

		List<Habitacion> listaHabitaciones = habitacionRepository.list();
		habitacion = ObtenerHabitacion(tipoHabitacion, listaHabitaciones);
		ocupada = comprobarOcupada(habitacion);
		double precio = calcularPrecio(PRECIO_NOCHE, numNoches);

		if (!ocupada) {

			reservaNueva = new Reserva(tipoPension, habitacion, cliente, checkIn, checkOut, extras, peticiones, precio);

			habitacion.setOcupada(true);
			habitacionRepository.update(habitacion);

			reservaRepository.add(reservaNueva);
		} else {
			Logger.getLogger(getClass().getName()).log(Level.INFO, "No hay habitaciones disponibles");
		}
		return reservaNueva;

	}

	public Habitacion ObtenerHabitacion(String tipoHabitacion, List<Habitacion> listaHabitaciones) {
		listaHabitaciones = habitacionRepository.list();
		Habitacion habitacionAReservar = null;
		for (Habitacion h : listaHabitaciones) {
			if (h.getTipoHabitacion().equals(tipoHabitacion)) {
				habitacionAReservar = h;
				break;
			}
		}
		return habitacionAReservar;
	}

	@Override
	public boolean comprobarOcupada(Habitacion habitacion) {
		boolean ocupada = habitacion.isOcupada();

		if (ocupada) {
			Logger.getLogger(getClass().getName()).log(Level.INFO, "No hay habitaciones disponibles");

		}
		return ocupada;
	}

	@Override
	public double calcularPrecio(double precio, int numNoches) {
		return numNoches * PRECIO_NOCHE;
	}

	@Override
	public void borrarReserva(Long id) {
		reservaRepository.delete(id);

	}

}
