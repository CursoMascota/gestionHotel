package es.cic.curso.curso04.mascota.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso04.mascota.dto.Habitacion;
import es.cic.curso.curso04.mascota.repository.HabitacionRepository;

@Service
@Transactional
public class HabitacionServiceImpl implements HabitacionService {

	@Autowired
	HabitacionRepository habitacionRepository;

	@Override
	public List<Habitacion> listar() {
		return habitacionRepository.list();
	}

	@Override
	public Habitacion insertarHabitacion(int numHabitacion, String tipoHabitacion, int numPersonas, boolean isOcupada) {
		Habitacion nuevaHabitacion = new Habitacion(numHabitacion, tipoHabitacion, numPersonas, isOcupada);
		habitacionRepository.add(nuevaHabitacion);
		return nuevaHabitacion;

	}

}
