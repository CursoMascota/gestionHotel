package es.cic.curso.curso04.mascota.controller;

import es.cic.curso.curso04.mascota.dto.Cliente;

public interface ClienteController {

	Cliente altaCliente(String nombre, String apellidos, String NIF, int telefono);

	void borrarCliente(Long id);

}
