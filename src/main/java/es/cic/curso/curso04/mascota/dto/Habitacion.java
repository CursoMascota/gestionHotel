package es.cic.curso.curso04.mascota.dto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import es.cic.curso.curso04.mascota.repository.Identificable;

@Entity
public class Habitacion implements Identificable<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "num_habitacion")
	private int numHabitacion;

	@Column(name = "tipo_habitacion")
	private String tipoHabitacion;

	@Column(name = "num_personas")
	private int numPersonas;

	private boolean isOcupada;

	@OneToMany(mappedBy = "habitacion")
	private List<Reserva> listaReservas = new ArrayList<>();

	public Habitacion() {
		super();
	}

	public Habitacion(int numHabitacion, String tipoHabitacion, int numPersonas, boolean isOcupada) {
		super();
		this.numHabitacion = numHabitacion;
		this.tipoHabitacion = tipoHabitacion;
		this.numPersonas = numPersonas;
		this.isOcupada = isOcupada;
	}

	public Habitacion(Long id, int numHabitacion, String tipoHabitacion, int numPersonas) {
		super();
		this.id = id;
		this.numHabitacion = numHabitacion;
		this.tipoHabitacion = tipoHabitacion;
		this.numPersonas = numPersonas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumHabitacion() {
		return numHabitacion;
	}

	public void setNumHabitacion(int numHabitacion) {
		this.numHabitacion = numHabitacion;
	}

	public String getTipoHabitacion() {
		return tipoHabitacion;
	}

	public void setTipoHabitacion(String tipoHabitacion) {
		this.tipoHabitacion = tipoHabitacion;
	}

	public int getNumPersonas() {
		return numPersonas;
	}

	public void setNumPersonas(int numPersonas) {
		this.numPersonas = numPersonas;
	}

	public boolean isOcupada() {
		return isOcupada;
	}

	public void setOcupada(boolean isOcupada) {
		this.isOcupada = isOcupada;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Habitacion other = (Habitacion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Habitacion [id=" + id + ", numHabitacion=" + numHabitacion + ", tipoHabitacion=" + tipoHabitacion
				+ ", numPersonas=" + numPersonas + ", isOcupada=" + isOcupada + ", listaReservas=" + listaReservas
				+ "]";
	}

}