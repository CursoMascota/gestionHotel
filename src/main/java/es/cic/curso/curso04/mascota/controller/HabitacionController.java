package es.cic.curso.curso04.mascota.controller;

import java.util.List;

import es.cic.curso.curso04.mascota.dto.Habitacion;

public interface HabitacionController {

	List<Habitacion> listar();

	Habitacion insertarHabitacion(int numHabitacion, String tipoHabitacion, int numPersonas, boolean isOcupada);

}