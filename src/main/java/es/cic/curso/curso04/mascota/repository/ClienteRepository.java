package es.cic.curso.curso04.mascota.repository;

import es.cic.curso.curso04.mascota.dto.Cliente;

public interface ClienteRepository extends IRepository<Long, Cliente> {

}
