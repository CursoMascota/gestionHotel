package es.cic.curso.curso04.mascota.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso04.mascota.dto.Habitacion;
import es.cic.curso.curso04.mascota.dto.Reserva;
import es.cic.curso.curso04.mascota.helper.TestHelper;
import es.cic.curso.curso04.mascota.repository.HabitacionRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:es/cic/curso/curso04/mascota/applicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class })
@Transactional
public class HabitacionServiceImplTest {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private HabitacionService habitacionService;

	@Autowired
	private HabitacionRepository habitacionRepository;

	@Autowired
	private TestHelper tHelper;

	Reserva reserva;

	@Before
	public void setUp() throws Exception {
		reserva = tHelper.generaReserva();
	}

	@Test
	public void testListar() {
		List<Habitacion> resultado = habitacionService.listar();
		assertTrue(resultado.size() >= 1);
	}

	@Test
	public void testInsertar() {
		Habitacion nueva = new Habitacion();
		nueva = habitacionService.insertarHabitacion(102, "doble", 2, false);
		nueva = habitacionRepository.read(nueva.getId());
		System.out.println(habitacionRepository.list());
		assertEquals(102, nueva.getNumHabitacion());
		assertEquals(2, habitacionRepository.list().size());

	}

	public void generaHabitaciones() {
		Habitacion habitacion1 = new Habitacion(102, "doble", 2, false);
		Habitacion habitacion2 = new Habitacion(103, "doble", 2, false);
		Habitacion habitacion3 = new Habitacion(104, "doble", 2, false);

		em.persist(habitacion1);
		em.persist(habitacion2);
		em.persist(habitacion3);
	}

}
