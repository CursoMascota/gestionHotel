package es.cic.curso.curso04.mascota.helper;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import es.cic.curso.curso04.mascota.dto.Cliente;
import es.cic.curso.curso04.mascota.dto.Habitacion;
import es.cic.curso.curso04.mascota.dto.Reserva;

@Repository
public class TestHelper {

	@PersistenceContext
	private EntityManager em;

	public Reserva generaReserva() {
		Habitacion habitacion = generaHabitacion();
		em.persist(habitacion);

		Cliente cliente = generaCliente();
		em.persist(cliente);

		Reserva reserva = new Reserva();
		reserva.setTipoPension("media");
		reserva.setHabitacion(habitacion);
		reserva.setCliente(cliente);
		reserva.setCheckIn("19/03/2017");
		reserva.setCheckOut("22/03/2017");
		reserva.setExtras(null);
		reserva.setPeticiones(null);
		reserva.setPrecioTotal(150.0);

		em.persist(reserva);
		return reserva;
	}

	public Cliente generaCliente() {
		Cliente cliente1 = new Cliente();
		cliente1.setNombre("pepe");
		cliente1.setApellidos("pepe");
		cliente1.setNIF("1234567Z");
		cliente1.setTelefono(123456789);

		em.persist(cliente1);

		return cliente1;
	}

	public Habitacion generaHabitacion() {
		Habitacion habitacion = new Habitacion();
		habitacion.setNumHabitacion(123);
		habitacion.setTipoHabitacion("doble");
		habitacion.setNumPersonas(2);
		habitacion.setOcupada(false);

		em.persist(habitacion);

		return habitacion;
	}
}