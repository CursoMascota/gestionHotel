package es.cic.curso.curso04.mascota.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso04.mascota.dto.Cliente;
import es.cic.curso.curso04.mascota.dto.Habitacion;
import es.cic.curso.curso04.mascota.dto.Reserva;
import es.cic.curso.curso04.mascota.helper.TestHelper;
import es.cic.curso.curso04.mascota.repository.ReservaRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:es/cic/curso/curso04/mascota/applicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class })
@Transactional
public class ReservaServiceImplTest {

	private static final double DELTA_PRECIO = 0.001;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ReservaRepository reservaRepository;

	@Autowired
	private ReservaService reservaService;

	@Autowired
	TestHelper tHelper;

	Habitacion habitacion;
	Cliente cliente;
	Reserva reserva;

	@Before
	public void setUp() throws Exception {

		limpiarReservas();
		reserva = tHelper.generaReserva();
		habitacion = tHelper.generaHabitacion();
	}

	@Test
	public void testCalcularPrecio() {
		double precioTotal = reservaService.calcularPrecio(50.0, 3);
		assertEquals(150.0, precioTotal, DELTA_PRECIO);

	}

	@Test
	public void testComprobarIsOcupada() {
		Reserva reservaRealizada = reservaService.reservar("doble", habitacion, cliente, null, null, null, null);
		Habitacion habitacionReservada = reservaRealizada.getHabitacion();
		boolean isOcupada = reservaService.comprobarOcupada(habitacionReservada);

		assertTrue(isOcupada);
	}

	@Test
	public void testComprobarNoOcupada() {
		boolean isOcupada = reservaService.comprobarOcupada(habitacion);
		assertFalse(isOcupada);
	}

	@Test
	public void testReservar() {
		int cantidadReservas = reservaRepository.list().size();
		Habitacion habitacionReservar = new Habitacion(456, "doble", 2, false);

		em.persist(habitacionReservar);
		reservaService.reservar("doble", habitacionReservar, cliente, null, null, null, null);

		assertEquals(cantidadReservas + 1, reservaRepository.list().size());
	}

	@Test
	public void testBorrar() {
		Reserva borrar = new Reserva();
		borrar = tHelper.generaReserva();
		reservaService.reservar("doble", habitacion, cliente, null, null, null, null);
		borrar = reservaRepository.read(borrar.getId());

		int cantidadHabitaciones = reservaRepository.list().size();

		reservaService.borrarReserva(borrar.getId());

		assertEquals(cantidadHabitaciones - 1, reservaRepository.list().size());
	}

	private void limpiarReservas() {
		List<Reserva> listaReservas = reservaRepository.list();
		for (Reserva reserva : listaReservas) {
			reservaRepository.delete(reserva);
		}
	}

}
