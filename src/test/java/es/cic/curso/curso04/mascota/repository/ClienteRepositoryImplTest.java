package es.cic.curso.curso04.mascota.repository;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.cic.curso.curso04.mascota.dto.Cliente;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:es/cic/curso/curso04/mascota/applicationContext.xml" })

public class ClienteRepositoryImplTest extends AbstractRepositoryImplTest<Long, Cliente> {

	@Autowired
	private ClienteRepository clienteRepository;

	@Before
	public void setUp() throws Exception {
	}

	@Override
	public Cliente getInstanceDeTParaNuevo() {
		Cliente cliente = new Cliente();
		cliente.setNombre("pepe");
		cliente.setApellidos("pepe");
		cliente.setNIF("1234567A");
		cliente.setTelefono(123456789);

		return cliente;
	}

	@Override
	public Cliente getInstanceDeTParaLectura() {
		Cliente cliente = new Cliente();
		cliente.setNombre("juan");
		cliente.setApellidos("juan");
		cliente.setNIF("1234567A");
		cliente.setTelefono(123456789);

		return cliente;
	}

	@Override
	public boolean sonDatosIguales(Cliente t1, Cliente t2) {
		if (t1 == null || t2 == null) {
			throw new UnsupportedOperationException("No puedo comparar nulos");
		}
		if (t1.getNombre() != t2.getNombre()) {
			return false;
		}
		if (t1.getApellidos() != t2.getApellidos()) {
			return false;
		}
		if (t1.getNIF() != t2.getNIF()) {
			return false;
		}
		if (t1.getTelefono() != t2.getTelefono()) {
			return false;
		}
		return true;
	}

	@Override
	public Long getClavePrimariaNoExistente() {
		return Long.MAX_VALUE;
	}

	@Override
	public Cliente getInstanceDeTParaModificar(Long clave) {
		Cliente cliente = getInstanceDeTParaLectura();
		cliente.setId(clave);
		cliente.setNombre("pedro");
		return cliente;
	}

	@Override
	public IRepository<Long, Cliente> getRepository() {
		return clienteRepository;
	}
}
