package es.cic.curso.curso04.mascota.controller;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso04.mascota.dto.Cliente;
import es.cic.curso.curso04.mascota.repository.ClienteRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:es/cic/curso/curso04/mascota/applicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class })
@Transactional
public class ClienteControllerImplTest {

	private Cliente cliente1;
	private Cliente cliente2;
	private Cliente cliente3;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private ClienteController clienteController;

	@Before
	public void setUp() throws Exception {

		limpiarClientes();
		inicializaBaseDeDatos();
	}

	@Test
	public void testInsertar() {
		Cliente nuevo = new Cliente();
		nuevo = clienteController.altaCliente("abc", "abc", "abc123", 1234569);
		nuevo = clienteRepository.read(nuevo.getId());

		assertEquals("abc", nuevo.getNombre());
		assertEquals(4, clienteRepository.list().size());
	}

	@Test
	public void testBorrar() {

		Cliente borrar = new Cliente();
		borrar = clienteController.altaCliente("abc", "abc", "abc123", 1234569);
		borrar = clienteRepository.read(borrar.getId());

		clienteController.borrarCliente(borrar.getId());
		assertEquals(3, clienteRepository.list().size());
	}

	private void inicializaBaseDeDatos() {
		cliente1 = new Cliente("juan", "juan", "1234567J", 123456789);
		cliente2 = new Cliente("pepe", "pepe", "1234567P", 123456789);
		cliente3 = new Cliente("pedro", "pedro", "1234567P", 123456789);

		em.persist(cliente1);
		em.persist(cliente2);
		em.persist(cliente3);

	}

	private void limpiarClientes() {
		List<Cliente> listaClientes = clienteRepository.list();
		for (Cliente cliente : listaClientes) {
			clienteRepository.delete(cliente);
		}
	}

}
