package es.cic.curso.curso04.mascota.repository;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.cic.curso.curso04.mascota.dto.Habitacion;
import es.cic.curso.curso04.mascota.dto.Reserva;
import es.cic.curso.curso04.mascota.helper.TestHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:es/cic/curso/curso04/mascota/applicationContext.xml" })

public class HabitacionRepositoryImplTest extends AbstractRepositoryImplTest<Long, Habitacion> {

	@Autowired
	private HabitacionRepository habitacionRepository;

	@Autowired
	private TestHelper tHelper;

	Reserva reserva;

	@Before
	public void setUp() throws Exception {
		reserva = tHelper.generaReserva();
	}

	@Override
	public Habitacion getInstanceDeTParaNuevo() {
		Habitacion habitacion = new Habitacion();
		habitacion.setNumHabitacion(101);
		habitacion.setTipoHabitacion("doble");
		habitacion.setNumPersonas(2);
		habitacion.setOcupada(false);

		return habitacion;
	}

	@Override
	public Habitacion getInstanceDeTParaLectura() {
		Habitacion habitacion = new Habitacion();
		habitacion.setNumHabitacion(102);
		habitacion.setTipoHabitacion("doble");
		habitacion.setNumPersonas(2);
		habitacion.setOcupada(false);

		return habitacion;
	}

	@Override
	public boolean sonDatosIguales(Habitacion t1, Habitacion t2) {
		if (t1 == null || t2 == null) {
			throw new UnsupportedOperationException("No puedo comparar nulos");
		}
		if (t1.getNumHabitacion() != t2.getNumHabitacion()) {
			return false;
		}
		if (t1.getTipoHabitacion() != t2.getTipoHabitacion()) {
			return false;
		}
		if (t1.getNumPersonas() != t2.getNumPersonas()) {
			return false;
		}
		if (t1.isOcupada() != t2.isOcupada()) {
			return false;
		}

		return true;
	}

	@Override
	public Long getClavePrimariaNoExistente() {
		return Long.MAX_VALUE;
	}

	@Override
	public Habitacion getInstanceDeTParaModificar(Long clave) {
		Habitacion habitacion = getInstanceDeTParaLectura();
		habitacion.setId(clave);
		habitacion.setNumHabitacion(200);
		return habitacion;
	}

	@Override
	public IRepository<Long, Habitacion> getRepository() {
		return habitacionRepository;
	}
}
