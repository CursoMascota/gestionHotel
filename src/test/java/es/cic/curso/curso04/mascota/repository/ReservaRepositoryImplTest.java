package es.cic.curso.curso04.mascota.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.cic.curso.curso04.mascota.dto.Cliente;
import es.cic.curso.curso04.mascota.dto.Habitacion;
import es.cic.curso.curso04.mascota.dto.Reserva;
import es.cic.curso.curso04.mascota.helper.TestHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:es/cic/curso/curso04/mascota/applicationContext.xml" })

public class ReservaRepositoryImplTest extends AbstractRepositoryImplTest<Long, Reserva> {

	@Autowired
	private ReservaRepository reservaRepository;
	@PersistenceContext
	protected EntityManager em;
	@Autowired
	private TestHelper tHelper;

	private Cliente cliente;
	private Habitacion habitacion;

	@Before
	public void setUp() throws Exception {
		cliente = tHelper.generaCliente();

		habitacion = tHelper.generaHabitacion();
		em.persist(habitacion);
		em.persist(cliente);
	}

	@Override
	public Reserva getInstanceDeTParaNuevo() {
		Reserva reserva = new Reserva();
		reserva.setTipoPension("media");
		reserva.setHabitacion(habitacion);
		reserva.setCliente(cliente);
		reserva.setCheckIn("hoy");
		reserva.setCheckOut("mañana");
		reserva.setExtras("null");
		reserva.setPeticiones("null");
		return reserva;
	}

	@Override
	public Reserva getInstanceDeTParaLectura() {
		Reserva reserva = new Reserva();
		reserva.setTipoPension("media");
		reserva.setHabitacion(habitacion);
		reserva.setCliente(cliente);
		reserva.setCheckIn("hoy");
		reserva.setCheckOut("mañana");
		reserva.setExtras("null");
		reserva.setPeticiones("null");

		return reserva;
	}

	@Override
	public boolean sonDatosIguales(Reserva t1, Reserva t2) {
		if (t1 == null || t2 == null) {
			throw new UnsupportedOperationException("No puedo comparar nulos");
		}
		if (t1.getTipoPension() != t2.getTipoPension()) {
			return false;
		}
		if (t1.getHabitacion() != t2.getHabitacion()) {
			return false;
		}
		if (t1.getCliente() != t2.getCliente()) {
			return false;
		}
		if (t1.getCheckIn() != t2.getCheckIn()) {
			return false;
		}
		if (t1.getCheckOut() != t2.getCheckOut()) {
			return false;
		}
		if (t1.getExtras() != t2.getExtras()) {
			return false;
		}
		if (t1.getPeticiones() != t2.getPeticiones()) {
			return false;
		}
		return true;
	}

	@Override
	public Long getClavePrimariaNoExistente() {
		return Long.MIN_VALUE;
	}

	@Override
	public Reserva getInstanceDeTParaModificar(Long clave) {
		Reserva reserva = getInstanceDeTParaLectura();
		reserva.setTipoPension("media");
		return reserva;
	}

	@Override
	public IRepository<Long, Reserva> getRepository() {
		return reservaRepository;
	}
}
