Proyecto MASCOTA:
Se va a llevar la gestión de un hotel.

Para ello: 

    - Se procederá dar de alta a clientes y una posterior baja.
    
    - Dar de alta las habitaciones habilitadas para ser reservadas.
    
    - Poder realizar una reserva (por el momento no se controlan fechas)